<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Store;
use App\Models\ProductVariant;
class HomeController extends Controller
{
    public function index(){
        $users = User::all();
        // $products = Store::find(1)->products;
        // dd($products);

        $variants = ProductVariant::all();
        // dd($variant->product->store->user->name);
        //Inverse Query

        // dd($variants);
        // foreach($variants as $variant){
        //     dd($variant->product->product_name);
        // }
        

        return view('index',compact('users','variants'));
    }
}
